Intro to Mathematica
====================

This repository contains a Mathematica notebook that has the slides for an
introductory course in using Mathematica. I encourage you to download the
repository directly or clone the git repo, and work through the examples as I
cover them. There are several places where external data files provided in
the repository are read, and these won't be found if you just download the
notebook.

You will need to have Mathematica installed to work through this. If you do
not, there is a pdf provided of these slides (without the Mathematica output)
as [IntroToMathematica.pdf](IntroToMathematica.pdf). The verison generated to
include the output is called
[IntroToMathematica_with_output.pdf](IntroToMathematica_with_output.pdf).

The provided notebook file was generated with version 11. If you use an earlier
version there may be a couple of places where function options don't work as
expected, but the vast majority should still work correctly.
