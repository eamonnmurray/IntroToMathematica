#!/bin/bash
# Script to parse out volume and etotal from an Abinit output file.
# Usage:
# ./EvVol.sh Outputfile

# Save outfile as the first argument.
outfile=$1

# Use awk to save a list of the energies.
etotals=$(awk '/etotal/{print $2}' $outfile)

# Save the volumes as an array - uses extra parentheses
volumes=($(awk '/Unit cell volume/{print $5}' $outfile))

ii=0
# Loop over the etotal elements, and output the corresponding volume
for etot in $etotals; do
  echo ${volumes[$ii]} $etot
  ii=$ii+1
done
