#!/bin/bash
# Script to parse out xcart and etotal from an Abinit output file.
# Usage:
# ./EvX.sh Outputfile

# Save outfile as the first argument.
outfile=$1

# Use awk to save a list of the energies.
etotals=$(awk '/etotal/{print $2}' $outfile)

# Save the xcarts as an array - uses extra parentheses
xcarts=($(awk 'BEGIN{p=0}
              /END DATASET/{p=1}
              /xcart/{if(p==1) print $2" "$3" "$4}' $outfile))

ii=0
# Loop over the etotal elements, and output xcart in groups of 3.
for etot in $etotals; do
  echo ${xcarts[$ii]} ${xcarts[$ii+1]} ${xcarts[$ii+2]} $etot
  ii=$ii+3
done
