#!/bin/bash
# Script to parse out pressure and lattice length from an Abinit output file.
# Usage:
# ./AvP.sh Outputfile

# Save outfile as the first argument.
outfile=$1

# Use awk to save a list of the energies.
acells=$(awk '/acell[0-9]/{print $2}' $outfile)

# Save the volumes as an array - uses extra parentheses
pressures=($(awk '/Pressure/{print $8}' $outfile))

ii=0
# Loop over the etotal elements, and output the corresponding volume
for acell in $acells; do
  echo ${pressures[$ii]} $acell
  ii=$ii+1
done
